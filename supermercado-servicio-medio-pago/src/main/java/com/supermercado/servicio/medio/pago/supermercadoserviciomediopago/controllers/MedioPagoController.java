package com.supermercado.servicio.medio.pago.supermercadoserviciomediopago.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.supermercado.commons.supermercadocommons.entity.MedioPago;
import com.supermercado.servicio.medio.pago.supermercadoserviciomediopago.models.service.IMedioPagoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MedioPagoController {
    
    @Autowired
    private IMedioPagoService medioPagoService;

    @RequestMapping(value = "/crear_medio_pago", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<?> crearMedioPago(@Valid @RequestBody MedioPago medioPago){
        Map<String, Object> response = new HashMap<>();
        try {
            medioPagoService.guardar(medioPago);
            response.put("Status", HttpStatus.CREATED.value());
            response.put("Message", "Creado exitosamente");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            response.put("Status", HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.put("Message", e.getMessage());
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/actualizar_medio_pago", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<?> actualizarMedioPago(@Valid @RequestBody MedioPago medioPago){
        Map<String, Object> response = new HashMap<>();
        try {
            medioPagoService.guardar(medioPago);
            response.put("Status", HttpStatus.OK.value());
            response.put("Message", "Actualizado exitosamente");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.put("Status", HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.put("Message", e.getMessage());
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/get_medio_pago")
    public @ResponseBody List<MedioPago> getMedioPago(HttpServletRequest request){
        Long id = null;
        String marca = null;
        List<MedioPago> lista_medio_pagos = new ArrayList<>();

        if(request.getParameter("id") != null && request.getParameter("id").equals("")){
            id = Long.parseLong(request.getParameter("id"));
            MedioPago medioPago = medioPagoService.findById(id);
            lista_medio_pagos.add(medioPago);
            return lista_medio_pagos;
        }

        if(request.getParameter("marca") != null && request.getParameter("marca").equals("")){
            marca = request.getParameter("marca");
            MedioPago medioPago = medioPagoService.findByMarca(marca);
            lista_medio_pagos.add(medioPago);
            return lista_medio_pagos;
        }

        lista_medio_pagos = medioPagoService.findAll();
        return lista_medio_pagos;
    }
}
