package com.supermercado.servicio.medio.pago.supermercadoserviciomediopago.models.service;

import com.supermercado.commons.supermercadocommons.entity.MedioPago;
import com.supermercado.commons.supermercadocommons.service.CommonServiceImpl;
import com.supermercado.servicio.medio.pago.supermercadoserviciomediopago.models.dao.IMedioPagoDao;

public class MedioPagoServiceImpl extends CommonServiceImpl<MedioPago, IMedioPagoDao> implements IMedioPagoService{

    @Override
    public MedioPago findByMarca(String marca) {
        return this.findByMarca(marca);
    }
    
}
