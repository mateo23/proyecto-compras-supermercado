package com.supermercado.servicio.medio.pago.supermercadoserviciomediopago.models.service;

import com.supermercado.commons.supermercadocommons.entity.MedioPago;
import com.supermercado.commons.supermercadocommons.service.ICommonService;

public interface IMedioPagoService extends ICommonService<MedioPago> {
    public MedioPago findByMarca(String marca);
}
