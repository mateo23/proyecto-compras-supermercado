package com.supermercado.servicio.medio.pago.supermercadoserviciomediopago;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupermercadoServicioMedioPagoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SupermercadoServicioMedioPagoApplication.class, args);
	}

}
