package com.supermercado.servicio.medio.pago.supermercadoserviciomediopago.models.dao;

import com.supermercado.commons.supermercadocommons.entity.MedioPago;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IMedioPagoDao extends PagingAndSortingRepository<MedioPago, Long> {
    
    @Query("SELECT mp FROM MedioPago mp WHERE mp.marca = ?1")
    public MedioPago findByMarca(String marca);
}
