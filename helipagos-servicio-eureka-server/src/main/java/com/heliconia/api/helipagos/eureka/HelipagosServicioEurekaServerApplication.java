package com.heliconia.api.helipagos.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class HelipagosServicioEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelipagosServicioEurekaServerApplication.class, args);
	}

}
