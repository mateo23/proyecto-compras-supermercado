package com.supermercado.servicio.compra.supermercadoserviciocompra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupermercadoServicioCompraApplication {

	public static void main(String[] args) {
		SpringApplication.run(SupermercadoServicioCompraApplication.class, args);
	}

}
