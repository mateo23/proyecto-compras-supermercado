package com.supermercado.servicio.producto.supermercadoservicioproducto.models.service;

import java.util.List;

import com.supermercado.commons.supermercadocommons.entity.Producto;
import com.supermercado.commons.supermercadocommons.service.ICommonService;

public interface IProductoService extends ICommonService<Producto> {
    public List<Producto> getProductos();
    public List<Producto> findByMarca(String marca);
    public List<Producto> findByNombre(String nombre);
}
