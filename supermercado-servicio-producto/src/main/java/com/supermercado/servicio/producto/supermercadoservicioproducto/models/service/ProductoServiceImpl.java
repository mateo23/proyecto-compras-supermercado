package com.supermercado.servicio.producto.supermercadoservicioproducto.models.service;

import java.util.List;

import com.supermercado.commons.supermercadocommons.entity.Producto;
import com.supermercado.commons.supermercadocommons.service.CommonServiceImpl;
import com.supermercado.servicio.producto.supermercadoservicioproducto.models.dao.IProductoDao;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
public class ProductoServiceImpl extends CommonServiceImpl<Producto, IProductoDao> implements IProductoService {

    @Override
    @Transactional(readOnly = true)
    public List<Producto> getProductos() {
        return this.getProductos();
    }

    @Override
    public List<Producto> findByMarca(String marca) {
        return this.findByMarca(marca);
    }

    @Override
    public List<Producto> findByNombre(String nombre) {
        return this.findByNombre(nombre);
    }
    
}
