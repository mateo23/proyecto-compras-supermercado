package com.supermercado.servicio.producto.supermercadoservicioproducto.models.dao;

import java.util.List;

import com.supermercado.commons.supermercadocommons.entity.Producto;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IProductoDao extends PagingAndSortingRepository<Producto, Long> {
    
    @Query("select p from Producto p where p.enable != false")
    public List<Producto> getProductos();

    @Query("select p from Producto p where p.marca = ?1")
    public List<Producto> findByMarca(String marca); 

    @Query("select p from Producto p where p.nombre = ?1")
    public List<Producto> findByNombre(String nombre);
}
