package com.supermercado.servicio.producto.supermercadoservicioproducto.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.supermercado.commons.supermercadocommons.entity.Producto;
import com.supermercado.servicio.producto.supermercadoservicioproducto.models.service.IProductoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductoController {
    
    @Autowired
    private IProductoService productoService;

    @RequestMapping(value = "/crear_producto", method = RequestMethod.POST)
    public ResponseEntity<?> guardarProducto(@RequestBody Producto producto) {
        Map<String, Object> response = new HashMap<String, Object>();
        try {
            producto.setEnabled(true);
            productoService.guardar(producto);
            response.put("Status", HttpStatus.CREATED.value());
            response.put("Message", "Creado correctamente");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            response.put("Status", HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.put("Message", e.getMessage());
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/eliminar_producto", method = RequestMethod.POST)
    public ResponseEntity<?> bajaProducto(@RequestBody Long id){
        Producto producto = productoService.findById(id);
        Map<String, Object> response = new HashMap<>();
        if(producto == null){
            response.put("status", HttpStatus.NOT_FOUND.value());
            response.put("message", "Producto no encontrado");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        producto.setEnabled(false);
        productoService.actualizar(producto);
        response.put("status", HttpStatus.OK.value());
        response.put("message", "Producto eliminado correctamente");

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/get_productos")
    public @ResponseBody List<Producto> getProducts(HttpServletRequest request) {
        Long id = null;
        String marca = null;
        String nombre = null;
        if(request.getParameter("id") != null && !request.getParameter("id").equals("")){
            id = Long.parseLong(request.getParameter("id"));
            List<Producto> productos = new ArrayList<Producto>();
            Producto producto = productoService.findById(id);
            productos.add(producto);
            return productos;
        }

        if(request.getParameter("marca") != null && !request.getParameter("marca").equals("")){
            marca = request.getParameter("marca");
            List<Producto> productos = productoService.findByMarca(marca);
            return productos;
        }

        if(request.getParameter("nombre") != null && !request.getParameter("nombre").equals("")){
            nombre = request.getParameter("nombre");
            List<Producto> productos = productoService.findByNombre(nombre);
            return productos;
        }
        
        return productoService.getProductos();
    }

}
