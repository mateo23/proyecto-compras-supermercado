package com.supermercado.servicio.producto.supermercadoservicioproducto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupermercadoServicioProductoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SupermercadoServicioProductoApplication.class, args);
	}

}
