package com.supermercado.commons.supermercadocommons.service;

import java.util.List;

public interface ICommonService<E> {
    public void guardar(E entity);
    public void actualizar(E entity);
    public E findById(Long id);
    public List<E> findAll();
}
