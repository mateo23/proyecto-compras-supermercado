package com.supermercado.commons.supermercadocommons.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

public class CommonServiceImpl<E, R extends PagingAndSortingRepository<E, Long>> implements ICommonService<E> {

    @Autowired
    protected R repository;

    @Override
    @Transactional
    public void guardar(E entity) {
        repository.save(entity);
    }

    @Override
    @Transactional
    public void actualizar(E entity) {
        repository.save(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public E findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<E> findAll() {
        return (List<E>) repository.findAll();
    }
    
}
