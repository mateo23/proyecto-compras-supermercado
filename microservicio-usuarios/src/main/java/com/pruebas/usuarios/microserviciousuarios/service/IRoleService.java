package com.pruebas.usuarios.microserviciousuarios.service;

import java.util.List;

import com.supermercado.commons.supermercadocommons.entity.Role;

public interface IRoleService {
    public void guardarRole(Role role);
    public void eliminarRole(Role role);
    public Role findById(Long id);
    public List<Role> findAll();
    public Role findByName(String name);
}
