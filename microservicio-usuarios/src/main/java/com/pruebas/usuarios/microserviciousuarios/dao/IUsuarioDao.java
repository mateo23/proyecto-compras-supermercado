package com.pruebas.usuarios.microserviciousuarios.dao;

import com.supermercado.commons.supermercadocommons.entity.Usuario;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUsuarioDao extends PagingAndSortingRepository<Usuario, Long> {
    public Usuario findByUsername(String username);
}
