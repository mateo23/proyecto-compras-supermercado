package com.pruebas.usuarios.microserviciousuarios.service;

import java.util.List;

import com.pruebas.usuarios.microserviciousuarios.dao.IRoleDao;
import com.supermercado.commons.supermercadocommons.entity.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleServiceImpl implements IRoleService{

    @Autowired
    private IRoleDao roleDao;

    @Override
    @Transactional
    public void guardarRole(Role role) {
        roleDao.save(role);
    }

    @Override
    @Transactional
    public void eliminarRole(Role role) {
        roleDao.delete(role);
    }

    @Override
    @Transactional(readOnly = true)
    public Role findById(Long id) {
        return roleDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Role> findAll() {
        return (List<Role>) roleDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Role findByName(String name) {
        return roleDao.findByName(name);
    }
    
}
