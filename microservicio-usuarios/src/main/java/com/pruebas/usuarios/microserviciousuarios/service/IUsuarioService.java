package com.pruebas.usuarios.microserviciousuarios.service;

import java.util.List;

import com.supermercado.commons.supermercadocommons.entity.Usuario;

public interface IUsuarioService {
    public void save(Usuario usuario);
    public void delete(Usuario usuario);
    public Usuario findById(Long id);
    public List<Usuario> findAll();
    public Usuario findByUsername(String username);
}
