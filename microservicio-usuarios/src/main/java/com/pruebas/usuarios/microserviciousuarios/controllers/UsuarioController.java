package com.pruebas.usuarios.microserviciousuarios.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pruebas.usuarios.microserviciousuarios.service.IRoleService;
import com.pruebas.usuarios.microserviciousuarios.service.IUsuarioService;
import com.supermercado.commons.supermercadocommons.entity.Role;
import com.supermercado.commons.supermercadocommons.entity.Usuario;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    protected final Log logger = LogFactory.getLog(this.getClass());
    
    @Autowired
    private IUsuarioService usuarioService;

    @Autowired
    private IRoleService roleService;

    @RequestMapping(value = "/crear_usuario", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<?> crearUsuarios(@RequestBody Usuario usuario){
        Map<String, Object> response = new HashMap<>();
        List<Role> roles = usuario.getRoles();
        logger.info("roles: " + roles.get(0).getId());
        List<Role> rolesNuevo = new ArrayList<>();
        if(roles == null || roles.isEmpty()){
            response.put("Status", HttpStatus.NOT_FOUND.value());
            response.put("Message", "Rol no encontrado");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        for (Role role : roles) {
            Role roleEnc = roleService.findById(role.getId());
            rolesNuevo.add(roleEnc);
        }
        
        usuario.setRoles(rolesNuevo);
        usuarioService.save(usuario);

        response.put("Status", HttpStatus.CREATED.value());
        response.put("Message", "El usuario " +usuario.getUsername()+ " creado correctamente");
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/crear_role", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<?> crearRole(@RequestBody Role role){
        roleService.guardarRole(role);
        Map<String, Object> response = new HashMap<>();
        response.put("Status", HttpStatus.CREATED.value());
        response.put("Message", "El role " +role.getName()+ "se creó correctamente");
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/ver_roles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Role> verRoles(){
        return roleService.findAll();
    }

    @RequestMapping(value = "/ver_usuarios", method = RequestMethod.GET)
    public @ResponseBody List<Usuario> verUsuarios(){
        return usuarioService.findAll();
    }

    @RequestMapping(value = "/ver_role/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Role buscarRole(@RequestParam("id") Long id){
        return roleService.findById(id);
    }

    @RequestMapping(value = "/actualizar_usuario", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<?> actualizarUsuarios(@RequestBody Usuario usuario){
        Map<String, Object> response = new HashMap<>();
        List<Role> roles = usuario.getRoles();
        List<Role> rolesNuevo = new ArrayList<>();
        if(roles == null || roles.isEmpty()){
            response.put("Status", HttpStatus.NOT_FOUND.value());
            response.put("Message", "Rol no encontrado");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        for (Role role : roles) {
            Role roleEnc = roleService.findById(role.getId());
            rolesNuevo.add(roleEnc);
        }
        
        usuario.setRoles(rolesNuevo);
        usuarioService.save(usuario);

        response.put("Status", HttpStatus.CREATED.value());
        response.put("Message", "El usuario " +usuario.getUsername()+ " actualizado correctamente");
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/buscar_username/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Usuario buscarPorUsername(@RequestParam("username") String username){
        if(username.equals("") || username == null){
            return null;
        }
        
        return usuarioService.findByUsername(username);
    }

}
