package com.pruebas.usuarios.microserviciousuarios.dao;

import com.supermercado.commons.supermercadocommons.entity.Role;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleDao extends PagingAndSortingRepository<Role, Long> {
    public Role findByName(String name);
}
