package com.supermercado.microservicio.oauth.supermercadomicroserviciooauth.clientes;

import com.supermercado.commons.supermercadocommons.entity.Usuario;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(name = "microservicio-usuarios")
public interface UsuarioFeingClient {
    @RequestMapping(value = "/buscar_username/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Usuario buscarPorUsername(@RequestParam("username") String username);
}
