package com.supermercado.microservicio.oauth.supermercadomicroserviciooauth.services;

import com.supermercado.commons.supermercadocommons.entity.Usuario;

public interface IUserService {
    public Usuario buscarPorUsername(String username);
}
