package com.supermercado.microservicio.oauth.supermercadomicroserviciooauth.services;

import java.util.List;
import java.util.stream.Collectors;

import com.supermercado.commons.supermercadocommons.entity.Usuario;
import com.supermercado.microservicio.oauth.supermercadomicroserviciooauth.clientes.UsuarioFeingClient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserDetailsService, IUserService {

    private Log log = LogFactory.getLog(this.getClass());

    @Autowired
    private UsuarioFeingClient usuarioFeingClient;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario user = usuarioFeingClient.buscarPorUsername(username);

        if(user == null){
            log.error("El usuario '"+ username + "' no existe");
            throw new UsernameNotFoundException("El usuario '"+ username + "' no existe");
        }

        List<GrantedAuthority> authorities = user.getRoles().stream()
            .map(role -> new SimpleGrantedAuthority(role.getName()))
            .peek(authority -> log.info("Role: "+authority.getAuthority()))
            .collect(Collectors.toList());
        return new User(user.getUsername(), user.getPassword(), user.getEnable(), true, true, true, 
        authorities);
    }

    @Override
    public Usuario buscarPorUsername(String username) {
        return usuarioFeingClient.buscarPorUsername(username);
    }

    
    
}
