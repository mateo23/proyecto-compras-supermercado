package com.supermercado.microservicio.oauth.supermercadomicroserviciooauth.security;

import java.util.HashMap;
import java.util.Map;

import com.supermercado.commons.supermercadocommons.entity.Usuario;
import com.supermercado.microservicio.oauth.supermercadomicroserviciooauth.services.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

@Component
public class InfoAdicionalToken implements TokenEnhancer{

    @Autowired
    private IUserService usuarioService;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        Map<String, Object> info = new HashMap<String, Object>();
        Usuario user = usuarioService.buscarPorUsername(authentication.getName());
        info.put("nombre", user.getNombre());
        info.put("apellido", user.getApellido());
        info.put("email", user.getEmail());
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);

        return accessToken;
    }
    
}
