package com.supermercado.servicio.supermercado.supermercadoserviciosupermercado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupermercadoServicioSupermercadoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SupermercadoServicioSupermercadoApplication.class, args);
	}

}
