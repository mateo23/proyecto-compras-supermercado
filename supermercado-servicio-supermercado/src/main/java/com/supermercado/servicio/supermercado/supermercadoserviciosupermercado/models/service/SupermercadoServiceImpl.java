package com.supermercado.servicio.supermercado.supermercadoserviciosupermercado.models.service;

import com.supermercado.commons.supermercadocommons.entity.Supermercado;
import com.supermercado.commons.supermercadocommons.service.CommonServiceImpl;
import com.supermercado.servicio.supermercado.supermercadoserviciosupermercado.models.dao.ISupermercadoDao;

import org.springframework.stereotype.Service;

@Service
public class SupermercadoServiceImpl extends CommonServiceImpl<Supermercado, ISupermercadoDao> implements ISupermercadoService{

    @Override
    public Supermercado findByNombre(String nombre) {
        return this.findByNombre(nombre);
    }
    
}
