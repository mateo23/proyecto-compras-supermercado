package com.supermercado.servicio.supermercado.supermercadoserviciosupermercado.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.supermercado.commons.supermercadocommons.entity.Supermercado;
import com.supermercado.servicio.supermercado.supermercadoserviciosupermercado.models.service.ISupermercadoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SupermercadoController {

    @Autowired
    private ISupermercadoService supermercadoService;
    
    @RequestMapping(value = "/crear_supermercado" , method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<?> crearSupermercado(@Valid @RequestBody Supermercado supermercado) {
        Map<String, Object> response = new HashMap<>();
        supermercadoService.guardar(supermercado);
        response.put("Status", HttpStatus.CREATED.value());
        response.put("Message", "Creado exitosamente");

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }


    public @ResponseBody ResponseEntity<?> actualizarSupermercado(@Valid @RequestBody Supermercado supermercado){
        Map<String, Object> response = new HashMap<>();
        supermercadoService.actualizar(supermercado);
        response.put("Status", HttpStatus.OK.value());
        response.put("Message", "Actualizado exitosamente");

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
    }

    public @ResponseBody List<Supermercado> getSupermercado(HttpServletRequest request) {

        Long id = null;
        String nombre = null;
        List<Supermercado> lista_supermercados = new ArrayList<Supermercado>();
        if(request.getParameter("id") == null || request.getParameter("id").equals("")){
            id = Long.parseLong(request.getParameter("id"));
            Supermercado supermercado = supermercadoService.findById(id);
            if(supermercado == null){
                return null;
            }
            lista_supermercados.add(supermercado);
            return lista_supermercados;
        }
        if(request.getParameter("nombre") == null || request.getParameter("nombre").equals("")){
            nombre = request.getParameter("nombre");
            Supermercado supermercado = supermercadoService.findByNombre(nombre);
            if(supermercado == null){
                return null;
            }
            lista_supermercados.add(supermercado);
            return lista_supermercados;
        }

        lista_supermercados = supermercadoService.findAll();
        return lista_supermercados;
    }
}
