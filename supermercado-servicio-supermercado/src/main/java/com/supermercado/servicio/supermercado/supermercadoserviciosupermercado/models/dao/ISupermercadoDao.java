package com.supermercado.servicio.supermercado.supermercadoserviciosupermercado.models.dao;

import com.supermercado.commons.supermercadocommons.entity.Supermercado;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISupermercadoDao extends PagingAndSortingRepository<Supermercado, Long> {
    
    @Query("SELECT s FROM Supermercado s WHERE s.nombre = ?1")
    public Supermercado findByNombre(String nombre);
}
