package com.supermercado.servicio.supermercado.supermercadoserviciosupermercado.models.service;

import com.supermercado.commons.supermercadocommons.entity.Supermercado;
import com.supermercado.commons.supermercadocommons.service.ICommonService;

public interface ISupermercadoService extends ICommonService<Supermercado> {
    public Supermercado findByNombre(String nombre);
}
